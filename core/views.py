from blog.models import Category


class CategoryContextMixin:
    extra_context = {'categories': Category.objects.all().prefetch_related('blogpost_set')}
