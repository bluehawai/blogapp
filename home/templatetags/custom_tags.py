from django import template
from blog.models import BlogPost

register = template.Library()


@register.filter
def filter_qs(qs, value):
    key, value = value.split(',')
    return qs.filter(**{key: value})
