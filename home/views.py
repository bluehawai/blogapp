from django.shortcuts import render
from django.views.generic import ListView

from blog.models import *

# Create your views here.
from core.views import CategoryContextMixin


class HomePageView(CategoryContextMixin, ListView):
    queryset = BlogPost.objects.order_by('pub_date')
    template_name = 'index.html'

