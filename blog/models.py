from django.db import models
from ckeditor.fields import RichTextField
from django import forms
from django.db.models import Choices
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.text import slugify


class Category(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True, verbose_name='Category')
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse_lazy('categories', args=[self.slug])


class BlogPost(models.Model):
    author = models.CharField(max_length=50, blank=True, null=True, verbose_name='Yazar')  # yazar ismi
    pub_date = models.DateField(verbose_name='Yayın Tarihi', default=timezone.now)  # yayım tarihi
    title = models.CharField(max_length=100, blank=False, verbose_name='Başlık')  # başlık
    slug = models.SlugField(unique=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, to_field='slug')
    content = RichTextField()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('blog_detail', args=[self.category.slug, self.slug])


class BlogHeadImage(models.Model):
    blogpost = models.OneToOneField(BlogPost, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='headimages/')


class BlogContentImage(models.Model):
    blogpost = models.ForeignKey(BlogPost, related_name='contentimages', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='contentimages')


class BlogComments(models.Model):
    blogpost = models.ForeignKey(BlogPost, related_name='comments', on_delete=models.CASCADE)
    name = models.CharField(max_length=30, blank=False, null=False, )
    email = models.EmailField(max_length=70, blank=False, null=False)
    massage = models.TextField(blank=False)
    comment_datetime = models.DateTimeField(auto_now_add=True)
