from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from blog.views import *

urlpatterns = [
    path('categories/<str:category>/<slug:slug>', BlogDetailsView.as_view(), name='blog_detail'),
    path('categories/<slug:slug>/', BlogListView.as_view(), name='categories')
]
