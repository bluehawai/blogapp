from django.contrib import admin
from .models import BlogPost, BlogHeadImage, BlogContentImage, BlogComments, Category


# Register your models here.


class BlogHeadInline(admin.StackedInline):
    model = BlogHeadImage
    extra = 1
    max_num = 1


class BlogContentInline(admin.TabularInline):
    model = BlogContentImage
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    pass


class BlogAdmin(admin.ModelAdmin):
    inlines = [BlogHeadInline, BlogContentInline]


admin.site.register(BlogPost, BlogAdmin)
admin.site.register(Category, CategoryAdmin)
