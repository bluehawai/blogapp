from django.shortcuts import render, HttpResponse
from django.views.generic import DetailView, ListView
from .models import BlogPost
from core.views import CategoryContextMixin


# Create your views here.


class BlogListView(CategoryContextMixin, ListView):
    paginate_by = 5
    queryset = BlogPost.objects.order_by('pub_date').prefetch_related(
        'contentimages').select_related('blogheadimage', 'category')

    template_name = 'categories.html'

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        return queryset.filter(category__slug=self.kwargs['slug'])


class BlogDetailsView(CategoryContextMixin, DetailView):
    model = BlogPost
    template_name = "blog-single.html"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        return queryset.filter(category=self.kwargs.get('category'))

    def get(self, *args, **kwargs):
        print(args, kwargs)
        return super(BlogDetailsView, self).get(*args, **kwargs)



